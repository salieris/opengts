package org.opengts.war.report.ifta;

import org.opengts.db.tables.Account;
import org.opengts.util.I18N;
import org.opengts.util.StringTools;
import org.opengts.war.report.*;
import org.opengts.war.report.ifta.DetailReport.IftaData;

/**
 * Created by lazar on 2/22/14.
 */

public class DetailLayout
        extends ReportLayout {
    private static DetailLayout reportDef = null;

    public static final String DATA_INDEX = "index";
    public static final String DATA_SUBDIVISION = "subdivision";
    public static final String DATA_SUBDIV_NAME = "subdivName";
    public static final String DATA_ENTER_DATE_TIME = "enterDateTime";
    public static final String DATA_ENTER_ODOMETER = "enterOdometerKm";
    public static final String DATA_EXIT_DATE_TIME = "exitDateTime";
    public static final String DATA_EXIT_ODOMETER = "exitOdometerKm";
    public static final String DATA_DISTANCE_TRAVELED = "distanceTraveledKm";

    public static ReportLayout getReportLayout() {
        if (reportDef == null) {
            reportDef = new DetailLayout();
        }
        return reportDef;
    }

    /**
     * <Column name="index"                       />
     * <Column name="subdivision"                 />
     * <Column name="subdivName"                  />
     * <Column name="enterDateTime"               />
     * <Column name="enterOdometerKm"       arg="1" />
     * <Column name="exitDateTime"                />
     * <Column name="exitOdometerKM"        arg="1" />
     * <Column name="distanceTraveled"    arg="1" />
     */
    protected static class IftaDataRow
            extends DataRowTemplate {
        public IftaDataRow() {
            super();

            // Index
            this.addColumnTemplate(new DataColumnTemplate(DATA_INDEX) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    return String.valueOf(rowNdx + 1);
                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    return "#";
                }
            });

            // State abbreviation
            this.addColumnTemplate(new DataColumnTemplate(DATA_SUBDIVISION) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    IftaData data = (IftaData)obj;
                    if (data.stateInfo == null)
                        return i18n.getString("IftaDetailLayout.Total","Total");
                    return data.getSubdivision();
                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    return i18n.getString("IftaDetailLayout.dataSubdivision","State\\nAbbrev.");
                }
            });

            // State Name
            this.addColumnTemplate(new DataColumnTemplate(DATA_SUBDIV_NAME) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    IftaData data = (IftaData)obj;
                    if (data.stateInfo == null)
                        return null;
                    return data.getSubdivisionName();
                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    return i18n.getString("IftaDetailLayout.dataSubdivName","State\\nName");
                }
            });

            // Enter date/time
            this.addColumnTemplate(new DataColumnTemplate(DATA_ENTER_DATE_TIME) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    IftaData data = (IftaData)obj;
                    if (data.stateInfo == null)
                        return null;
                    return data.getEnterDateTime();
                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    return i18n.getString("IftaDetailLayout.dataEnterDateTime","Enter\nDate/Time") + "\n${timezone}";
                }
            });

            // Enter odometer
            this.addColumnTemplate(new DataColumnTemplate(DATA_ENTER_ODOMETER) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    IftaData data = (IftaData)obj;
                    double  odom = data.getEnterOdometerKm(); // kilometers
                    String   arg = rc.getArg();

                    odom = Account.getDistanceUnits(rd.getAccount()).convertFromKM(odom);
                    return formatDouble(odom, arg, "0");
                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    return i18n.getString("IftaDetailLayout.dataEnterOdometer","Enter Odometer\nValue") + "\n${distanceUnits}";
                }
            });

            // Exit Date/Time
            this.addColumnTemplate(new DataColumnTemplate(DATA_EXIT_DATE_TIME) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    IftaData data = (IftaData)obj;
                    if (data.stateInfo == null)
                        return null;
                    return data.getExitDateTime();
                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    return i18n.getString("IftaDetailLayout.dataExitDateTime","Exit\nDate/Time") + "\n${timezone}";
                }
            });

            // Exit odometer
            this.addColumnTemplate(new DataColumnTemplate(DATA_EXIT_ODOMETER) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    IftaData data = (IftaData)obj;
                    double  odom = data.getExitOdometerKm(); // kilometers
                    String   arg = rc.getArg();

                    odom = Account.getDistanceUnits(rd.getAccount()).convertFromKM(odom);
                    return formatDouble(odom, arg, "0");
                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    return i18n.getString("IftaDetailLayout.dataExitOdometer","Exit Odometer\nValue") + "\n${distanceUnits}";
                }
            });

            // Distance Traveled
            this.addColumnTemplate(new DataColumnTemplate(DATA_DISTANCE_TRAVELED) {
                public Object getColumnValue(int rowNdx, ReportData rd, ReportColumn rc, Object obj) {
                    IftaData data = (IftaData)obj;
                    double  odom = data.getDistanceTraveledKm(); // kilometers
                    String   arg = rc.getArg();

                    odom = Account.getDistanceUnits(rd.getAccount()).convertFromKM(odom);
                    return formatDouble(odom, arg, "0");

                }

                public String getTitle(ReportData rd, ReportColumn rc) {
                    I18N i18n = rd.getPrivateLabel().getI18N(DetailLayout.class);
                    return i18n.getString("IftaDetailLayout.dataDistanceTraveled","Distance\nTraveled") + "\n${distanceUnits}";
                }
            });
        }
    }

    /**
     * ** Standard singleton constructor
     */
    private DetailLayout() {
        super();
        this.setDataRowTemplate(new IftaDataRow());
    }

    /* format double value */
    protected static Object formatDouble(double value, String arg, String dftFmt)
    {
        String fmt = dftFmt;
        if (StringTools.isBlank(arg)) {
            // default format
            fmt = dftFmt;
        } else
        if (arg.startsWith("#")) {
            // explicit format (ie. "#0.0000")
            fmt = arg.substring(1);
        } else {
            // format alias, "arg" represents number of decimal points
            switch (arg.charAt(0)) {
                case '0': fmt = "0"          ; break;
                case '1': fmt = "0.0"        ; break;
                case '2': fmt = "0.00"       ; break;
                case '3': fmt = "0.000"      ; break;
                case '4': fmt = "0.0000"     ; break;
                case '5': fmt = "0.00000"    ; break;
                case '6': fmt = "0.000000"   ; break;
                case '7': fmt = "0.0000000"  ; break;
                case '8': fmt = "0.00000000" ; break;
                case '9': fmt = "0.000000000"; break;
                default : fmt = dftFmt       ; break;
            }
        }
        return StringTools.format(value, fmt);
    }

}