package org.opengts.war.report.ifta;

import org.brongus.shapefile.bcross.State;
import org.brongus.shapefile.bcross.StateInfo;
import org.opengts.db.tables.Account;
import org.opengts.db.tables.EventData;
import org.opengts.war.report.*;
import org.opengts.war.tools.RequestProperties;

import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

/**
 * Created by lazar on 2/22/14.
 */
public class DetailReport
        extends ReportData {

    protected DBDataIterator iftaDataIterator = null;
    protected List<IftaData> iftaDetailedDataList = null;

    public DetailReport(ReportEntry rptEntry, RequestProperties reqState, ReportDeviceList devList)
            throws ReportException {
        super(rptEntry, reqState, devList);

        /* has account */
        if (this.getAccount() == null) {
            throw new ReportException("Account-ID not specified");
        }

        /* has at least one device */
        if (this.getDeviceCount() < 1) {
            throw new ReportException("No Devices specified");
        }
    }

    public boolean isSingleDeviceOnly() {
        return true;
    }

    /**
     * ** Gets the bound ReportLayout singleton instance for this report
     * ** @return The bound ReportLayout
     */
    public static ReportLayout GetReportLayout() {
        // bind the report format to this data
        return DetailLayout.getReportLayout();
    }

    /**
     * ** Gets the bound ReportLayout singleton instance for this report
     * ** @return The bound ReportLayout
     */
    public ReportLayout getReportLayout() {
        // bind the report format to this data
        return GetReportLayout();
    }

    /**
     * ** Creates and returns an iterator for the row data displayed in the body of this report.
     * ** @return The body row data iterator
     */
    public DBDataIterator getBodyDataIterator() {
    	return getIftaDataIterator();
    }

    /**
     * ** Creates and returns an iterator for the row data displayed in the total rows of this report.
     * ** @return The total row data iterator
     */
    public DBDataIterator getTotalsDataIterator() {
        List<IftaData> iftaDataList;
        List<IftaData> totalsList = new Vector<IftaData>();
        IftaData totalData = new IftaData();
        
        totalData.setEnterTimestamp(0);
        
        iftaDataList = _getIftaDataList();
        for (IftaData data : iftaDataList)
        {
        	totalData.setEnterOdometerKm(totalData.getEnterOdometerKm() + data.getEnterOdometerKm());
        	totalData.setExitOdometerKm(totalData.getExitOdometerKm() + data.getExitOdometerKm());
        }
        totalsList.add(totalData);
        
        return new ArrayDataIterator(totalsList.toArray());
    }

    private DBDataIterator getIftaDataIterator() {
    	
    	if (iftaDataIterator == null)
    	{
    		List<IftaData> iftaDataList = _getIftaDataList();
    		iftaDataIterator = new ArrayDataIterator(iftaDataList.toArray(new IftaData[iftaDataList.size()]));
    	}
        
        return iftaDataIterator;
    }
    
    protected List<IftaData> _getIftaDataList()
    {
        IftaData iftaData;
        EventData[] eventData;

        State state;
        double event_latitue;
        double event_longitude;
        StateInfo prevStateInfo = null;
        StateInfo currentStateInfo;

        if (iftaDetailedDataList == null)
        {
        	eventData = this.getEventData(null);
        	iftaDetailedDataList = new Vector<IftaData>();
        	state = new State();
        	
        	for (EventData ev : eventData) {
        		event_latitue = ev.getLatitude();
        		event_longitude = ev.getLongitude();
        		currentStateInfo = state.getStateInfoForPoint(event_latitue, event_longitude);
        		
        		if (prevStateInfo == null) {
        			iftaData = new IftaData();
        			iftaData.setEnterTimestamp(ev.getTimestamp())
        			.setEnterOdometerKm(ev.getOdometerKM())
        			.setStateInfo(currentStateInfo);
        			iftaDetailedDataList.add(iftaData);
        		} else {
        			iftaData = iftaDetailedDataList.get(iftaDetailedDataList.size() - 1);
        			iftaData.setExitTimestamp(ev.getTimestamp())
        			.setExitOdometerKm(ev.getOdometerKM());
        			if (!currentStateInfo.stateId.equals(prevStateInfo.stateId)) {
        				
        				iftaData = new IftaData();
        				iftaData.setEnterTimestamp(ev.getTimestamp())
        				.setEnterOdometerKm(ev.getOdometerKM())
        				.setStateInfo(currentStateInfo);
        				iftaDetailedDataList.add(iftaData);
        			}
        		}
        		prevStateInfo = currentStateInfo;
        		
        	}
        }
        
        return iftaDetailedDataList;
    }


    // ------------------------------------------------------------------------

    /**
     * Created by lazar on 3/29/14.
     */
    public class IftaData {
        protected StateInfo stateInfo;
        protected long enterTimestamp = 0;
        protected long exitTimestamp = 0;
        protected double enterOdometerKm = 0;
        protected double exitOdometerKM = 0;
        protected double distanceTraveled = 0;

        public StateInfo getStateInfo() {
            return stateInfo;
        }

        public long getEnterTimestamp() {
            return enterTimestamp;
        }

        public String getEnterDateTime() {
            return getTimestampString(this.enterTimestamp);
        }

        public String getExitDateTime() {
            return getTimestampString(this.exitTimestamp);
        }

        public long getExitTimestamp() {
            return exitTimestamp;
        }

        public double getEnterOdometerKm() {
            return enterOdometerKm;
        }

        public double getExitOdometerKm() {
            return exitOdometerKM;
        }

        public String getSubdivision() {
            return stateInfo.stateAbbreviation;
        }

        public String getSubdivisionName() {
            return stateInfo.stateName;
        }


        /**
         * ** Gets the String representation of the timestamp of this event
         * ** @return The String representation of the timestamp of this event
         */
        private String getTimestampString(long ts) {
            Account acct = getAccount();
            TimeZone tmz = getTimeZone();
            return EventData.getTimestampString(ts, acct, tmz, null);
        }

        public double getDistanceTraveledKm() {
        	
        	if (distanceTraveled == 0)
        	{
				return exitOdometerKM - enterOdometerKm;
        	}
        	return distanceTraveled;
        }

        public IftaData setStateInfo(StateInfo info) {
            stateInfo = info;
            return this;
        }

        public IftaData setEnterTimestamp(long enterTs) {
            enterTimestamp = enterTs;
            return this;
        }

        public IftaData setExitTimestamp(long exitTs) {
            exitTimestamp = exitTs;
            return this;
        }

        public IftaData setEnterOdometerKm(double enterOdm) {
            enterOdometerKm = enterOdm;
            return this;
        }
        
        public IftaData setExitOdometerKm(double exitOdm) {
            exitOdometerKM = exitOdm;
            return this;
        }
        
        public IftaData addDistanceTraveled(double distance) {
            distanceTraveled = getDistanceTraveledKm() + distance;
            return this;
        }
    }
}
