package org.opengts.war.report.ifta;

import org.opengts.war.report.DBDataIterator;
import org.opengts.war.report.ReportDeviceList;
import org.opengts.war.report.ReportEntry;
import org.opengts.war.report.ReportException;
import org.opengts.war.tools.RequestProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class StateMileageSummaryReport extends DetailReport {
	
	protected List<IftaData> iftaSummaryDataList = null;

	public StateMileageSummaryReport(ReportEntry rptEntry,
			RequestProperties reqState, ReportDeviceList devList)
			throws ReportException {
		super(rptEntry, reqState, devList);
	}

    protected List<IftaData> _getIftaDataList()
    {
    	HashMap<String, IftaData> iftaDataStateMap = new HashMap<String, IftaData>();
    	
        if (iftaSummaryDataList == null)
        {
        	List<IftaData> allData = super._getIftaDataList();
        	
        	for (IftaData data : allData)
        	{
        		if (iftaDataStateMap.get(data.stateInfo.stateId) == null)
        		{
        			iftaDataStateMap.put(data.stateInfo.stateId, data);
        		}
        		else
        		{
        			iftaDataStateMap.get(data.stateInfo.stateId)
        							.addDistanceTraveled(data.getDistanceTraveledKm());
        		}
        	}
        	iftaSummaryDataList = new ArrayList<IftaData>(iftaDataStateMap.values()); 
        }
        
        return iftaSummaryDataList;
    }
    
    /**
     * ** Creates and returns an iterator for the row data displayed in the total rows of this report.
     * ** @return The total row data iterator
     */
    public DBDataIterator getTotalsDataIterator() {
        List<IftaData> iftaDataList;
        List<IftaData> totalsList = new Vector<IftaData>();
        IftaData totalData = new IftaData();
        
        totalData.setEnterTimestamp(0);
        
        iftaDataList = _getIftaDataList();
        for (IftaData data : iftaDataList)
        {
        	totalData.addDistanceTraveled(data.getDistanceTraveledKm());
        }
        totalsList.add(totalData);
        
        return new ArrayDataIterator(totalsList.toArray());
    }

}
